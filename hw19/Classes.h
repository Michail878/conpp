
class Animal
{
public:
	virtual void voice();
	

};
class Dog : public Animal
{
public:
	void voice() override;
	

};
class Cat : public Animal
{
public:
	void voice() override;
	
};
class Snail : public Animal
{
public:
	void voice() override;
	
};